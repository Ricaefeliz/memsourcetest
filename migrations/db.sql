CREATE TABLE `option` (
  `o_id` int(11) NOT NULL AUTO_INCREMENT,
  `o_user_id` int(11) NOT NULL,
  `o_username` varchar(255) DEFAULT NULL,
  `o_password` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`o_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `option` (`o_id`, `o_user_id`, `o_username`, `o_password`) VALUES
(1, 1, NULL, NULL);