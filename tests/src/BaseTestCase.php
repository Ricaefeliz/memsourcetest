<?php

namespace Test;

use Nette\DI\Container;
use Tester\TestCase;


abstract class BaseTestCase extends TestCase
{


    /** @var Container */
    protected $container;



    protected function createContainer()
    {
        global $container;
        $this->container = $container;
    }



    protected function setUp()
    {
        parent::setUp();
        $this->createContainer();
    }
}