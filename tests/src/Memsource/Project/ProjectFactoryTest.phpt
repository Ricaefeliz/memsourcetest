<?php

namespace Test\Memsource\Project;

use Tester\Assert;
use Tester\TestCase;


require_once __DIR__ . '/../../bootstrap.php';

class ProjectFactoryTest extends TestCase
{


    public function testCreate()
    {
        $name = 'Test project';
        $status = 'NEW';
        $sourceLang = 'en_1';
        $targetLangs = ['es_23', 'eb_22'];

        $projectFactory = new ProjectFactory();
        $project = $projectFactory->create($name, $status, $sourceLang, $targetLangs);

        Assert::same($name, $project->getName());
        Assert::same($status, $project->getStatus());
        Assert::same($sourceLang, $project->getSourceLanguage());
        Assert::same($targetLangs, $project->getTargetLanguages());
    }
}

(new ProjectFactoryTest())->run();