<?php

namespace Test\Memsource\Option;

use Test\BaseTestCase;
use Tester\Assert;


require_once __DIR__ . '/../../bootstrap.php';

/**
 * @author Dusan Mlynarcik <dusan.mlynarcik@email.cz>
 */
class OptionSaveFacadeTest extends BaseTestCase
{


    /** @var Option|null */
    protected $option;

    /** @var OptionRepository|null */
    protected $optionRepo;



    public function setUp()
    {
        parent::setUp();

        //save test option
        $this->option = $option = new Option();
        $option->setUserId(20);
        $option->setUserName('testusername');
        $option->setPassword('testpassword');

        $optionRepoFactory = $this->container->getByType(OptionRepositoryFactory::class);
        $this->optionRepo = $optionRepoFactory->create();
        $this->optionRepo->saveNew($option);
    }



    public function testUpdate()
    {
        $userId = (int)$this->option->getUserId();
        $userName = 'newUserName';
        $password = 'newpassword';

        $updateFacadeFactory = $this->container->getByType(OptionSaveFacadeFactory::class);
        $updateFacade = $updateFacadeFactory->create();
        $updateFacade->update($userId, $userName, $password);

        //load option from storage
        $optionFromStorage = $this->optionRepo->getOneByUserId($userId);

        Assert::same((int)$this->option->getId(), $optionFromStorage->getId());
        Assert::same($userName, $optionFromStorage->getUserName());
        Assert::same($password, $optionFromStorage->getPassword());
    }



    public function testUpdateForUnknownUserId()
    {
        $userId = (int)($this->option->getId() + 20);
        $userName = 'newUserName';
        $password = 'newpassword';

        $updateFacadeFactory = $this->container->getByType(OptionSaveFacadeFactory::class);
        $updateFacade = $updateFacadeFactory->create();

        Assert::exception(function () use ($userId, $userName, $password, $updateFacade) {
            $updateFacade->update($userId, $userName, $password);
        }, OptionSaveFacadeException::class, sprintf('Option for user with id "%d" not found.', $userId));
    }



    public function tearDown()
    {
        parent::tearDown();

        //delete test option
        $this->optionRepo->delete($this->option);
    }
}

(new OptionSaveFacadeTest())->run();