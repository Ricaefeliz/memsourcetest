<?php

namespace Test\Memsource\Project;

use GuzzleHttp\Exception\RequestException;
use Psr\Http\Message\ResponseInterface;
use Test\Memsource\API\Responses\ErrorResponse;
use Test\Memsource\Login\Login;
use Test\Memsource\Login\LoginException;
use Test\Memsource\Option\OptionNotFoundException;
use Test\Memsource\Option\OptionRepositoryFactory;


/**
 * @author Dusan Mlynarcik <dusan.mlynarcik@email.cz>
 */
class ProjectFacade
{


    /** @var OptionRepositoryFactory */
    protected $optionRepoFactory;



    public function __construct(OptionRepositoryFactory $optionRepositoryFactory)
    {
        $this->optionRepoFactory = $optionRepositoryFactory;
    }



    /**
     * @param $userId int
     * @return array|Project[]
     * @throws ProjectFacadeException
     */
    public function findByUserId($userId)
    {
        try {
            $optionRepo = $this->optionRepoFactory->create();
            $option = $optionRepo->getOneByUserId($userId);

            $client = new \GuzzleHttp\Client(['base_uri' => 'https://cloud.memsource.com/web/api/v3/']);

            $login = new Login();
            $loggedIdentity = $login->authenticate($client, $option);

            $projects = $client->get('project/list', [
                'query' => [
                    'token' => $loggedIdentity->getToken()
                ]
            ]);

            return $this->createProjects($projects);
        } catch (OptionNotFoundException $exception) {
            throw new ProjectFacadeException($exception->getMessage());
        } catch (RequestException $exception) {
            $response = \GuzzleHttp\json_decode($exception->getResponse()->getBody(), TRUE);
            throw new ProjectFacadeException((new ErrorResponse($response))->getMessage());
        } catch (LoginException $exception){
            throw new ProjectFacadeException($exception->getMessage());
        }
    }



    /**
     * @return array|Project[]
     */
    protected function createProjects(ResponseInterface $response)
    {
        if ($response->getStatusCode() !== 200) {
            throw new \BadMethodCallException('Response must have code 200.');
        }

        $projectObjects = [];
        $projectArray = \GuzzleHttp\json_decode($response->getBody(), TRUE);
        $projectFactory = new ProjectFactory();
        foreach ($projectArray as $project) {
            $projectObjects[] = $projectFactory->create($project['name'],
                $project['status'], $project['sourceLang'], $project['targetLangs']);
        }

        return $projectObjects;
    }

}