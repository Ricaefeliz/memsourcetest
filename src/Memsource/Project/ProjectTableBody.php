<?php

namespace Test\Memsource\Project;

/**
 * @author Dusan Mlynarcik <dusan.mlynarcik@email.cz>
 *
 * POZN.: Jen pro účely testu.
 */
class ProjectTableBody
{


    /** @var Project[] */
    protected $projects;



    public function __construct(array $projects)
    {
        $this->projects = $projects;
    }



    /**
     * @return string
     */
    public function getBody()
    {
        $body = '';
        foreach ($this->projects as $project) {
            $body .= sprintf('<tr><td>%s</td><td>%s</td><td>%s</td><td>%s</td></tr>',
                $project['name'], $project['status'], $project['sourceLanguage'], implode(',', $project['targetLanguages']));
        }
        return $body;
    }
}