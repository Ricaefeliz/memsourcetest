<?php

namespace Test\Memsource\Project;

/**
 * @author Dusan Mlynarcik <dusan.mlynarcik@email.cz>
 */
interface ProjectFacadeFactory
{


    /**
     * @return ProjectFacade
     */
    public function create();
}