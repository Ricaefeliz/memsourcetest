<?php

namespace Test\Memsource\Project;

/**
 * @author Dusan Mlynarcik <dusan.mlynarcik@email.cz>
 */
class ProjectFactory
{


    /**
     * @param $name string
     * @param $status string
     * @param $sourceLanguage string
     * @param $targetLanguages array
     * @return Project
     */
    public function create($name, $status, $sourceLanguage, $targetLanguages)
    {
        $project = new Project($name);
        $project->setStatus($status);
        $project->setSourceLanguage($sourceLanguage);
        $project->setTargetLanguages($targetLanguages);

        return $project;
    }
}