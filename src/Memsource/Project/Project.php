<?php

namespace Test\Memsource\Project;

/**
 * @author Dusan Mlynarcik <dusan.mlynarcik@email.cz>
 */
class Project implements \ArrayAccess
{


    /** @var string */
    protected $name;

    /** @var string|null */
    protected $status;

    /** @var string|null */
    protected $sourceLanguage;

    /** @var array */
    protected $targetLanguages = [];



    public function __construct($name)
    {
        $this->name = $name;
    }



    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }



    /**
     * @param $status string
     * @return self
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }



    /**
     * @return string|null
     */
    public function getStatus()
    {
        return $this->status;
    }



    /**
     * @return null|string
     */
    public function getSourceLanguage()
    {
        return $this->sourceLanguage;
    }



    /**
     * @param string $sourceLanguage
     * @return self
     */
    public function setSourceLanguage($sourceLanguage)
    {
        $this->sourceLanguage = $sourceLanguage;
        return $this;
    }



    /**
     * @return array
     */
    public function getTargetLanguages()
    {
        return $this->targetLanguages;
    }



    /**
     * @param array $targetLanguages
     * @return self
     */
    public function setTargetLanguages($targetLanguages)
    {
        $this->targetLanguages = $targetLanguages;
        return $this;
    }

    /***************** ArrayAccess interface *****************/

    /**
     * @inheritdoc
     */
    public function offsetExists($offset)
    {
        return property_exists($this, $offset);
    }



    /**
     * @inheritdoc
     */
    public function offsetGet($offset)
    {
        return $this->{'get' . ucfirst($offset)}();
    }



    /**
     * @inheritdoc
     * @throws ProjectException
     */
    public function offsetSet($offset, $value)
    {
        throw new ProjectException('You can not set value through array. Use "set" method.');
    }



    /**
     * @inheritdoc
     * @throws ProjectException
     */
    public function offsetUnset($offset)
    {
        throw new ProjectException('You can not do unset action.');
    }
}