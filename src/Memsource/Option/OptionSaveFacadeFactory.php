<?php

namespace Test\Memsource\Option;

/**
 * @author Dusan Mlynarcik <dusan.mlynarcik@email.cz>
 */
interface OptionSaveFacadeFactory
{


    /**
     * @return OptionSaveFacade
     */
    public function create();
}