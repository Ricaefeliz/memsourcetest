<?php

namespace Test\Memsource\Option;

/**
 * @author Dusan Mlynarcik <dusan.mlynarcik@email.cz>
 */
class OptionFactory
{


    /**
     * @param $id int
     * @param null $userName
     * @param null $password
     * @return Option
     */
    public function create($id, $userName = NULL, $password = NULL)
    {
        $option = new Option();
        $option->setId($id);
        $option->setUserName($userName);
        $option->setPassword($password);

        return $option;
    }
}