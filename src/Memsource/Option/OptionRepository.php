<?php

namespace Test\Memsource\Option;

use Nette\Database\Context;
use Nette\Database\Table\IRow;


/**
 * @author Dusan Mlynarcik <dusan.mlynarcik@email.cz>
 *
 * POZN.: V rámci testu repozitář řeší i zodpovědnost mapperu.
 */
class OptionRepository
{


    /** @var string */
    const TABLE = 'option';

    /** @var Context */
    protected $database;



    public function __construct(Context $context)
    {
        $this->database = $context;
    }



    /**
     * @param $userId int
     * @return Option|null
     */
    public function findOneOptionByUserId($userId)
    {
        $result = $this->database->table(self::TABLE)
            ->where('o_user_id = ?', $userId)
            ->fetch();

        if (!$result) {
            return NULL;
        }

        return $this->createOption($result);
    }



    /**
     * @param $userId int
     * @return Option
     * @throws OptionNotFoundException
     */
    public function getOneByUserId($userId)
    {
        $result = $this->database->table(self::TABLE)
            ->where('o_user_id = ?', $userId)
            ->fetch();

        if (!$result) {
            throw new OptionNotFoundException(sprintf('Option for user with id "%d" not found.', $userId));
        }

        return $this->createOption($result);
    }



    /**
     * @param Option $option
     * @return Option
     * @throws OptionRepositoryException
     */
    public function saveNew(Option $option)
    {
        if ($option->getId() !== NULL) {
            throw new OptionRepositoryException('For update a option use update method.');
        }

        $row = $this->database->table(self::TABLE)
            ->insert([
                'o_user_id' => $option->getUserId(),
                'o_username' => $option->getUserName(),
                'o_password' => $option->getPassword()
            ]);
        $option->setId($row['o_id']);

        return $option;
    }



    /**
     * @param Option $option
     * @return Option
     * @throws OptionRepositoryException
     */
    public function update(Option $option)
    {
        if ($option->getId() === NULL) {
            throw new OptionRepositoryException('You can update only option which exists already.');
        }

        $this->database->table(self::TABLE)
            ->where('o_id = ?', $option->getId())
            ->update([
                'o_username' => $option->getUserName(),
                'o_password' => $option->getPassword()
            ]);

        return $option;
    }



    /**
     * @param Option $option
     * @return bool
     * @throws OptionRepositoryException
     */
    public function delete(Option $option)
    {
        if ($option->getId() === NULL) {
            throw new OptionRepositoryException('You can only delete option which exists already.');
        }

        $this->database->table(self::TABLE)
            ->where('o_id = ?', $option->getId())
            ->delete();

        return TRUE;
    }



    /**
     * @param $row IRow
     * @return Option
     */
    protected function createOption(IRow $row)
    {
        $optionFactory = new OptionFactory();
        return $optionFactory->create($row['o_id'], $row['o_username'] ?: NULL, $row['o_password'] ?: NULL);
    }
}