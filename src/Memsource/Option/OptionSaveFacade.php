<?php

namespace Test\Memsource\Option;

/**
 * @author Dusan Mlynarcik <dusan.mlynarcik@email.cz>
 */
class OptionSaveFacade
{


    /** @var OptionRepositoryFactory */
    protected $optionRepositoryFactory;



    public function __construct(OptionRepositoryFactory $optionRepositoryFactory)
    {
        $this->optionRepositoryFactory = $optionRepositoryFactory;
    }



    /**
     * @param $userId int
     * @param $userName string
     * @param $password string
     * @return Option
     * @throws OptionSaveFacadeException
     */
    public function update($userId, $userName, $password)
    {
        try {
            $optionRepo = $this->optionRepositoryFactory->create();
            $option = $optionRepo->getOneByUserId($userId);

            //update
            $updateService = new OptionUpdateService();
            $updateService->update($option, $userName, $password);

            //save
            $optionRepo->update($option);

            return $option;
        } catch (OptionNotFoundException $exception) {
            throw new OptionSaveFacadeException($exception->getMessage());
        }
    }
}