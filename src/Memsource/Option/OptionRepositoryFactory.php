<?php

namespace Test\Memsource\Option;

/**
 * @author Dusan Mlynarcik <dusan.mlynarcik@email.cz>
 */
interface OptionRepositoryFactory
{


    /**
     * @return OptionRepository
     */
    public function create();
}