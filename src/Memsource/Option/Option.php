<?php

namespace Test\Memsource\Option;

/**
 * @author Dusan Mlynarcik <dusan.mlynarcik@email.cz>
 */
class Option
{


    /** @var int */
    const PASSWORD_MINIMAL_LENGTH = 5;

    /** @var int */
    protected $id;

    /** @var int|null */
    protected $userId;

    /** @var string|null */
    protected $userName;

    /** @var string|null */
    protected $password;



    /**
     * @param $id int
     * @return self
     * @throws OptionException
     */
    public function setId($id)
    {
        if ($this->id !== NULL) {
            throw new OptionException('You can not change id.');
        }
        $this->id = $id;
        return $this;
    }



    /**
     * @return int|null
     */
    public function getId()
    {
        return $this->id;
    }



    /**
     * @param $id int
     * @return self
     */
    public function setUserId($id)
    {
        $this->userId = $id;
        return $this;
    }



    /**
     * @return int|null
     */
    public function getUserId()
    {
        return $this->userId;
    }



    /**
     * @param $userName string
     * @return self
     */
    public function setUserName($userName)
    {
        $this->userName = $userName;
        return $this;
    }



    /**
     * @return string|null
     */
    public function getUserName()
    {
        return $this->userName;
    }



    /**
     * @param $password string
     * @return self
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }



    /**
     * @return string|null
     */
    public function getPassword()
    {
        return $this->password;
    }
}