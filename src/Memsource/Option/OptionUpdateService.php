<?php

namespace Test\Memsource\Option;

/**
 * @author Dusan Mlynarcik <dusan.mlynarcik@email.cz>
 */
class OptionUpdateService
{


    /**
     * @param $option Option
     * @param $userName string
     * @param $password string
     * @return Option
     */
    public function update(Option $option, $userName, $password)
    {
        $option->setUserName($userName);
        $option->setPassword($password);

        return $option;
    }
}