<?php

namespace Test\Memsource\Login;

/**
 * @author Dusan Mlynarcik <dusan.mlynarcik@email.cz>
 */
class LoggedIdentity
{


    /** @var int */
    protected $id;

    /** @var string */
    protected $token;



    public function __construct($id, $token)
    {
        $this->id = $id;
        $this->token = $token;
    }



    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }



    /**
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }
}