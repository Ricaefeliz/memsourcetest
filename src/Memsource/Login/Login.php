<?php

namespace Test\Memsource\Login;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Psr\Http\Message\ResponseInterface;
use Test\Memsource\API\Responses\ErrorResponse;
use Test\Memsource\Option\Option;


/**
 * @author Dusan Mlynarcik <dusan.mlynarcik@email.cz>
 */
class Login
{


    /**
     * @param Client $client
     * @param Option $option
     * @return LoggedIdentity
     * @throws LoginException
     */
    public function authenticate(Client $client, Option $option)
    {
        if ($option->getUserName() === NULL || $option->getPassword() === NULL) {
            throw new LoginException('You must first set username and password for your memsource account.');
        }

        try {
            $response = $client->request('POST', 'auth/login', [
                'query' => [
                    'userName' => $option->getUserName(),
                    'password' => $option->getPassword()
                ]
            ]);
            return $this->createIdentity($response);
        } catch (RequestException $exception) {
            $response = \GuzzleHttp\json_decode($exception->getResponse()->getBody(), TRUE);
            throw new LoginException((new ErrorResponse($response))->getMessage());
        }
    }



    /**
     * @param $response ResponseInterface
     * @return LoggedIdentity
     */
    protected function createIdentity(ResponseInterface $response)
    {
        if ($response->getStatusCode() !== 200) {
            throw new \BadMethodCallException('Response must have code 200.');
        }
        $user = \GuzzleHttp\json_decode($response->getBody(), TRUE);

        return new LoggedIdentity($user['user']['id'], $user['token']);
    }

}