<?php

namespace Test\Memsource\Components\ProjectList;

/**
 * @author Dusan Mlynarcik <dusan.mlynarcik@email.cz>
 */
interface ProjectListFactory
{


    /**
     * @return ProjectList
     */
    public function create();
}