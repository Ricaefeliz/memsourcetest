<?php

namespace Test\Memsource\Components\ProjectList;

use Nette\Application\Responses\TextResponse;
use Nette\Application\UI\Control;
use Test\Memsource\Project\ProjectFacadeException;
use Test\Memsource\Project\ProjectFacadeFactory;
use Test\Memsource\Project\ProjectTableBody;


/**
 * @author Dusan Mlynarcik <dusan.mlynarcik@email.cz>
 */
class ProjectList extends Control
{


    /** @var ProjectFacadeFactory */
    protected $projectFacadeFactory;



    public function __construct(ProjectFacadeFactory $projectFacadeFactory)
    {
        $this->projectFacadeFactory = $projectFacadeFactory;
    }



    /**
     * @return void
     */
    public function handleLoadProjects()
    {
        if ($this->presenter->isAjax()) {
            try {
                //load projects
                $projectFacade = $this->projectFacadeFactory->create();
                $projects = $projectFacade->findByUserId(1);

                if (!$projects) {
                    $this->presenter->sendResponse(new TextResponse('No project.'));
                }

                $tableBody = new ProjectTableBody($projects);
                $this->presenter->sendResponse(new TextResponse($tableBody->getBody()));
            } catch (ProjectFacadeException $exception) {
                $this->presenter->sendResponse(new TextResponse($exception->getMessage()));
            }
        }
    }



    public function render()
    {
        $this->template->setFile(__DIR__ . '/default.latte');
        $this->template->render();
    }
}