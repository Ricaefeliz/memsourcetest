<?php

namespace Test\Memsource\Components\OptionForm;

use Nette\Application\UI\Form;
use Nette\Database\Context;
use Test\Memsource\Option\OptionSaveFacadeException;
use Test\Memsource\Option\OptionSaveFacadeFactory;


/**
 * @author Dusan Mlynarcik <dusan.mlynarcik@email.cz>
 */
class OptionFormSuccess
{


    /** @var Context */
    protected $database;

    /** @var OptionSaveFacadeFactory */
    protected $optionSaveFacadeFactory;



    /**
     * OptionFormSuccess constructor.
     * @param Context $context
     * @param OptionSaveFacadeFactory $optionSaveFacadeFactory
     */
    public function __construct(Context $context,
                                OptionSaveFacadeFactory $optionSaveFacadeFactory)
    {
        $this->database = $context;
        $this->optionSaveFacadeFactory = $optionSaveFacadeFactory;
    }



    /**
     * @param Form $form
     * @param OptionForm $optionForm
     */
    public function execute(Form $form, OptionForm $optionForm)
    {
        $values = $form->getValues();
        $presenter = $optionForm->getPresenter();

        try {
            $this->database->beginTransaction();
            $saveFacade = $this->optionSaveFacadeFactory->create();
            $saveFacade->update(1, $values->userName, $values->password);
            $this->database->commit();

            $presenter->flashMessage('Option was save.', 'success');
            $presenter->redirect('this');
        } catch (OptionSaveFacadeException $exception) {
            $this->database->rollBack();
            $presenter->flashMessage($exception->getMessage());
        }
    }
}