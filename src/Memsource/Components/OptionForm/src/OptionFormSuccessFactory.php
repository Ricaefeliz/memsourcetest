<?php

namespace Test\Memsource\Components\OptionForm;

/**
 * @author Dusan Mlynarcik <dusan.mlynarcik@email.cz>
 */
interface OptionFormSuccessFactory
{


    /**
     * @return OptionFormSuccess
     */
    public function create();
}