<?php

namespace Test\Memsource\Components\OptionForm;

/**
 * @author Dusan Mlynarcik <dusan.mlynarcik@email.cz>
 */
interface OptionFormFactory
{


    /**
     * @return OptionForm
     */
    public function create();
}