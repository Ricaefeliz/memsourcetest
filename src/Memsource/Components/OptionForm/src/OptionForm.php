<?php

namespace Test\Memsource\Components\OptionForm;

use Nette\Application\UI\Control;
use Nette\Application\UI\Form;
use Test\Memsource\Option\Option;
use Test\Memsource\Option\OptionRepositoryFactory;


/**
 * @author Dusan Mlynarcik <dusan.mlynarcik@email.cz>
 */
class OptionForm extends Control
{


    /** @var OptionFormSuccessFactory */
    protected $formSuccessFactory;

    /** @var OptionRepositoryFactory */
    protected $optionRepoFactory;

    /** @var int test id of user */
    protected $userId = 1;



    public function __construct(OptionFormSuccessFactory $optionFormSuccessFactory,
                                OptionRepositoryFactory $optionRepositoryFactory)
    {
        $this->formSuccessFactory = $optionFormSuccessFactory;
        $this->optionRepoFactory = $optionRepositoryFactory;
    }



    /**
     * @return Form
     */
    public function createComponentForm()
    {
        $form = new Form();
        $form->addText('userName', 'Username*')
            ->setRequired('Username is required.');
        $form->addPassword('password', 'Password*')
            ->setRequired('Password is required.')
            ->addRule(Form::MIN_LENGTH, sprintf('The password must have minimal %d letters.', Option::PASSWORD_MINIMAL_LENGTH), Option::PASSWORD_MINIMAL_LENGTH);
        $form->addSubmit('submit', 'Save');
        $form->onSuccess[] = function (Form $form) {
            $formSuccess = $this->formSuccessFactory->create();
            $formSuccess->execute($form, $this);
        };
        $this->setDefaultValues($form);

        return $form;
    }



    /**
     * @param $form Form
     * @return Form
     */
    protected function setDefaultValues(Form $form)
    {
        //try find option for actual user
        $optionRepo = $this->optionRepoFactory->create();
        $option = $optionRepo->findOneOptionByUserId($this->userId);

        if ($option) {
            $form->setDefaults([
                'userName' => $option->getUserName(),
                'password' => $option->getPassword()
            ]);
        }

        return $form;
    }



    public function render()
    {
        $this->template->setFile(__DIR__ . '/default.latte');
        $this->template->render();
    }
}