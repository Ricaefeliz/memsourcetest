<?php

namespace Test\Memsource\API\Responses;

/**
 * @author Dusan Mlynarcik <dusan.mlynarcik@email.cz>
 */
interface IResponse
{


    /**
     * @return string
     */
    public function getMessage();
}