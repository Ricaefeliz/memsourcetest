<?php

namespace Test\Memsource\API\Responses;

/**
 * @author Dusan Mlynarcik <dusan.mlynarcik@email.cz>
 */
class ErrorResponse implements IResponse
{


    /** @var array */
    protected $response;



    public function __construct(array $response)
    {
        $this->checkStructure($response);
        $this->response = $response;
    }



    /**
     * @param $response array
     * @return void
     */
    protected function checkStructure(array $response)
    {
        $errorCode = 'errorCode';
        $errorDescription = 'errorDescription';

        try {
            if (!array_key_exists($errorCode, $response)) {
                throw new \Exception($errorCode);
            }
            if (!array_key_exists($errorDescription, $response)) {
                throw new \Exception($errorDescription);
            }
        } catch (\Exception $exception) {
            throw new \InvalidArgumentException(sprintf('Missing "%s" in response of error.', $exception->getMessage()));
        }
    }



    /**
     * @return string
     */
    public function getMessage()
    {
        if($this->response['errorCode'] === 'AuthInvalidCredentials'){
            return 'Username or password is not valid.';
        }
        return $this->response['errorDescription'];
    }
}