<?php

namespace App\Presenters;

use Nette\Application\UI\Presenter;
use Test\Memsource\Components\OptionForm\OptionForm;
use Test\Memsource\Components\OptionForm\OptionFormFactory;
use Test\Memsource\Components\ProjectList\ProjectList;
use Test\Memsource\Components\ProjectList\ProjectListFactory;


/**
 * @author Dusan Mlynarcik <dusan.mlynarcik@email.cz>
 */
class MemsourcePresenter extends Presenter
{


    /** @var OptionFormFactory */
    protected $optionFormFactory;

    /** @var ProjectListFactory */
    protected $projectListFactory;



    public function __construct(OptionFormFactory $optionFormFactory,
                                ProjectListFactory $projectListFactory)
    {
        parent::__construct();
        $this->optionFormFactory = $optionFormFactory;
        $this->projectListFactory = $projectListFactory;
    }



    /**
     * @return OptionForm
     */
    public function createComponentOptionForm()
    {
        return $this->optionFormFactory->create();
    }



    /**
     * @return ProjectList
     */
    public function createComponentProjectList()
    {
        return $this->projectListFactory->create();
    }
}