<?php

require __DIR__ . '/../vendor/autoload.php';

$configurator = new Nette\Configurator;

//$configurator->setDebugMode('23.75.345.200'); // enable for your remote IP
$configurator->enableTracy(__DIR__ . '/../log');

$configurator->setTimeZone('Europe/Prague');
$configurator->setTempDirectory(__DIR__ . '/../temp');

$configurator->createRobotLoader()
	->addDirectory(__DIR__)
    ->addDirectory(__DIR__ . '/../src')
	->register();

$configurator->addConfig(__DIR__ . '/config/config.neon');
if($_SERVER['SERVER_ADDR'] === '::1'){
    $configurator->addConfig(__DIR__ . '/config/config.local.neon');
}else{
    $configurator->addConfig(__DIR__ . '/config/config.production.neon');
}
$configurator->addConfig(__DIR__ . '/config/config.services.neon');

$container = $configurator->createContainer();

return $container;
